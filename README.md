# Local
    export SPRING_CONFIG_LOCATION=/Users/developer/Desktop/MiDesk/WS/DeepSource/9.WOM/wom-fuse-aaa/application.properties
    export GOOGLE_APPLICATION_CREDENTIALS=/Users/developer/Desktop/MiDesk/WS/DeepSource/9.WOM/wom-fuse-aaa/google-service-acount-file.json

# OpenShift
    SPRING_CONFIG_LOCATION=/opt/application.properties
    GOOGLE_APPLICATION_CREDENTIALS=/opt/google-service-acount-file.json


# Crear mensajes en el topico

```bash
./oc run kafka-producer -ti --image=registry.redhat.io/amq7/amq-streams-kafka-27-rhel7:1.7.0 --rm=true --restart=Never -- bin/kafka-console-producer.sh --broker-list my-cluster-kafka-bootstrap:9092 --topic my-topic
```